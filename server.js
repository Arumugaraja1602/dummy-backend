const express = require('express');
const app = express();
const port = 3000;
const mongoose = require('mongoose');


app.listen(port, () => {
    console.log(`Listening on port ${port}`);
})

// Mongodb connection
mongoose.connect('mongodb://localhost:27017/dummy',
    { useUnifiedTopology: true, useNewUrlParser: true })
    .then(() => console.log('Database Connected Successfully'))
    .catch((e) => console.log('DB Connection Error' + e.message))

app.use(express.urlencoded({ extended: true }));
app.use(express.json());



app.get('/', (req, res) => {
    res.send('helo world');
})

const file1Router = require('./api/routes/file1.api');

app.use('/file1', file1Router);