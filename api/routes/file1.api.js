const express = require('express');
const router = express.Router();
const file1Model = require('../models/file1');

router.get('/getAll', (req, res) => {
    file1Model.find((err, data) => {
        console.log('req',req.body);
        // console.log('res',res);
        if(err){
            console.log('data', data);
            res.send({status: 500, message: 'Unable to get'});
        } else {
            res.json(data);
        }
    });
});

router.get('/getOne/:id', (req, res) => {
    file1Model.findOne({'_id': req.params.id}, (err, data) =>{
        if(err){
            res.send({status: 500, message: 'Unable to get'});
        } else {
            res.json(data);
        }
    });
});

router.post('/create', (req, res) => {
    file1Model.create(req.body, (err, data) => {
        console.log(req.body);
        if(err){
            res.send({status: 401, message: 'Unable to create'});
        } else {
            res.json(data);
        }
    });
});

router.put('/update/:id', (req, res) => {
    file1Model.findByIdAndUpdate(req.params.id, {
        $set: req.body
    }, (err, data) => {
        if(err){
            res.send({status: 401, message: 'Unable to update'});
        } else {
            res.json(data);
        }
    });
});

router.delete('/delete/:id', (req, res) => {
    file1Model.findByIdAndRemove(req.params.id, (err, data) => {
        if(err){
            res.send({status: 403, message: 'Unable to delete'});
        } else {
            res.json(data);
        }
    });
});

module.exports = router;