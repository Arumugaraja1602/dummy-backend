const mongoose = require('mongoose');

const fileOneSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    emailId: {
        type: String,
        required: true
    },
    mobileNo: {
        type: String,
        required: true
    }
});

const fileOneModel = mongoose.model('FileOne', fileOneSchema);

module.exports = fileOneModel;